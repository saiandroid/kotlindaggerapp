package com.happy

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.bobuyan.example.demokotlin.R
import com.happy.di.AppComponent
import com.happy.di.AppConfig
import com.happy.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class App : Application(), LifecycleObserver, HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
        handleUnCaughtCrash()

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)


    }

    private fun handleUnCaughtCrash() {
       /* CaocConfig.Builder.create()
            .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT)
            .enabled(true)//default: true
            .logErrorOnRestart(true) //default: true
            .minTimeBetweenCrashesMs(2000) //default: 3000
            .errorDrawable(R.drawable.ic_bbyn_logo) //default: bug image
            .errorActivity(CrashActivity::class.java) //default: null (default error activity)
            .apply()*/
    }





    private fun initializeDagger() {
        appComponent = AppInjector.init(this)
    }



    companion object {

        lateinit var appComponent: AppComponent
        var isRootedDevice: Boolean = false
        var isForeground = true

        var language = "ar"
        val isRTL: Boolean
            get() = language == "ar"


    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        //App in background
        isForeground = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        // App in foreground
        isForeground = true
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }






}