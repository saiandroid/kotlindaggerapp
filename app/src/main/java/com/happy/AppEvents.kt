package com.happy

object AppEvents {

    object StartLoading {
        fun œ(): StartLoading {
            return StartLoading
        }
    }

    object Exception {
        fun œ(): Exception {
            return Exception
        }
    }

    class UpdateTitleBar(title: String?) {
        var title = title
    }

    class UpdateNotificationCount(count: Int) {
        var count = count
    }






    object IsNotOk {
        fun œ(): IsNotOk {
            return IsNotOk
        }
    }



    object ShowAlertDialog {
        fun œ(): ShowAlertDialog {
            return ShowAlertDialog
        }
    }



}