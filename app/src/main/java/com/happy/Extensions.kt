package com.happy

import org.greenrobot.eventbus.EventBus

val eventBus: EventBus by lazy { EventBus.getDefault() }
