package com.happy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bobuyan.example.demokotlin.R
import com.happy.data.ArticleViewModel
import com.happy.data.Error
import com.happy.data.model.Article
import com.happy.data.model.Envelope
import com.happy.network.APIs
import com.happy.network.NetworkRepository
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: ArticleViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ArticleViewModel::class.java)




        viewModel.getArticles()?.observe(this@MainActivity,object : UIObserver<Envelope<List<Article>>>(){
                        override fun onSuccess(data: Envelope<List<Article>>?) {
                            data?.let {

                            }
                        }

                        override fun onFailed(error: Error?) {
                            error?.let {

                            }
                        }

                    })










    }
}
