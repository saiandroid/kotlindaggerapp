package com.happy

import androidx.lifecycle.Observer
import com.happy.data.Error
import com.happy.data.Resource
import com.happy.data.Status

abstract class UIObserver<T> : Observer<Resource<T?>> {
    final override fun onChanged(resource: Resource<T?>) {
        when(resource.status) {
            Status.LOADING -> onLoading()
            Status.SUCCESS -> {
                onSuccess(resource.data) // subscribed in SecuredFragment
            }
            else -> onFailed(resource.error)
        }
    }

    abstract fun onSuccess(data: T?)
    open fun onFailed(error: Error?) {}
    open fun onLoading(){}
}