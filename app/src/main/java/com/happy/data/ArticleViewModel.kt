package com.happy.data

import androidx.lifecycle.ViewModel
import com.happy.OpenForTesting
import com.happy.network.NetworkRepository
import javax.inject.Inject


@OpenForTesting
class ArticleViewModel @Inject constructor(val networkRepository: NetworkRepository): ViewModel(){

    fun getArticles() = networkRepository.getArticles()

}