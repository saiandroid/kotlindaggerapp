package com.happy.data

import com.google.gson.GsonBuilder

class Error(
    val code: Int = 0,
    private val description: String? = null,
    var isClean: Boolean? = null,
    private val reference: Any? = null,
    private val results: Any? = null,
    private val data: Any? = null,
    val title: String? = null) {



    fun status(): Int {
        return code
    }

    fun message(): String? {
        return description
    }

    fun getResults(): Any {
        return reference ?: (results ?: "")
    }

    fun parse(`object`: Any?): Error? {
        if (`object` == null) return null
        val gson = GsonBuilder().serializeNulls().create()
        val data = gson.toJson(`object`)
        return gson.fromJson(data, Error::class.java)
    }

    companion object {
        const val OK = 0
        const val EXCEPTION = -1
        const val FAILED = 1

    }

    override fun equals(other: Any?): Boolean {
        if (other !is Error) return false

        return other.code == this.code
                && other.description == this.description
                && other.isClean == this.isClean
                && other.reference == this.reference
                && other.results == this.results
                && other.data == this.data
                && other.title == this.title
    }
}