package com.happy.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}