package com.happy.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Success() : Parcelable {

    @SerializedName("messageHeader")
    var message: String? = null
    @SerializedName("successMsg1")
    var messageDesc1: String? = null
    @SerializedName("successMsg2")
    var messageDesc2: String? = null
    @SerializedName("extraData")
    var extra: Extra? = null

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(message)
        dest?.writeString(messageDesc1)
        dest?.writeString(messageDesc2)
    }

    override fun describeContents(): Int {
        return 0
    }

    constructor(parcel: Parcel) : this() {
        message = parcel.readString()
        messageDesc1 = parcel.readString()
        messageDesc2 = parcel.readString()
    }

    companion object {

        fun prepare(message: String?, messageDesc: String?): Success {
            val success = Success()
            success.message = message
            success.messageDesc1 = messageDesc
            return success
        }

        @JvmField
        val CREATOR = object : Parcelable.Creator<Success> {
            override fun createFromParcel(parcel: Parcel) = Success(parcel)

            override fun newArray(size: Int) = arrayOfNulls<Success>(size)
        }
    }


    class Extra() : Parcelable {

        @SerializedName("key")
        var label: String? = null
        @SerializedName("value")
        var valueToShare: String? = null
        @SerializedName("shareMsg")
        var shareMsg: String? = null


        constructor(parcel: Parcel) : this() {
            label = parcel.readString()
            valueToShare = parcel.readString()
            shareMsg = parcel.readString()
        }

        companion object CREATOR : Parcelable.Creator<Extra> {
            override fun createFromParcel(parcel: Parcel): Extra {
                return Extra(parcel)
            }

            override fun newArray(size: Int): Array<Extra?> {
                return arrayOfNulls(size)
            }
        }

        override fun writeToParcel(dest: Parcel?, flags: Int) {
            dest?.writeString(label)
            dest?.writeString(valueToShare)
            dest?.writeString(shareMsg)
        }

        override fun describeContents(): Int {
            return 0
        }
    }
}