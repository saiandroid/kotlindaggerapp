package com.happy.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

public class Article() : Parcelable {

    var accountNumber: String? = null
    @SerializedName("ibanNumber")
    var iban: String? = null
    @SerializedName("accountNickname")
    var accountType: String? = null

    protected constructor(`in`: Parcel) : this() {

        accountNumber = `in`.readString()
        iban = `in`.readString()
        accountType = `in`.readString()

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(accountNumber)
        dest.writeString(iban)
        dest.writeString(accountType)

    }



    companion object CREATOR : Parcelable.Creator<Article> {

        val CREATOR: Parcelable.Creator<Article> = object : Parcelable.Creator<Article> {
            override fun createFromParcel(`in`: Parcel): Article {
                return Article(`in`)
            }

            override fun newArray(size: Int): Array<Article?> {
                return arrayOfNulls(size)
            }
        }

        override fun createFromParcel(parcel: Parcel): Article {
            return Article(parcel)
        }

        override fun newArray(size: Int): Array<Article?> {
            return arrayOfNulls(size)
        }
    }



}