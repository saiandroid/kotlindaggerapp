package com.happy.data.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.annotations.SerializedName
import com.happy.data.Error
import com.happy.data.Success
import com.happy.network.ConfirmationData

class Envelope<T> {
    @SerializedName("error")
    var error: Error? = null
    var confirmationData: ConfirmationData? = null
    var data: T? = null
    @SerializedName("successMessages")
    var success: Success? = null

    constructor()
    constructor(data: T?): this() {
        this.data = data
        this.error = Error()
        this.error?.isClean = true
    }

    fun <R> map(call: () -> R?): Envelope<R> {
        val new = Envelope<R>()
        new.error = this.error
        new.confirmationData = this.confirmationData
        new.success = this.success
        new.data = call()

        return new
    }

    companion object {

        fun <T> toLiveData(data: T?): LiveData<Envelope<T>> {
            val liveData = MutableLiveData<Envelope<T>>()
            val envelope = Envelope<T>()
            envelope.data = data
            envelope.error = Error()
            envelope.error?.isClean = true
            liveData.value = envelope
            return liveData
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Envelope<*>) return false

        return other.data == this.data
                && other.confirmationData == this.confirmationData
                && other.success == this.success
                && other.error == this.error

    }
}