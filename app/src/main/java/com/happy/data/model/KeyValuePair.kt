package com.happy.data.model

import android.os.Parcel
import android.os.Parcelable

class KeyValuePair : Parcelable {

    var key: String? = null
    var value: Any? = null

    constructor(key: String, value: Any) {
        this.key = key
        this.value = value
    }


    protected constructor(`in`: Parcel) {
        key = `in`.readString()
        value = `in`.readParcelable(Any::class.java.classLoader)
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(key)
        //  dest.writeParcelable(value,flags);
    }

    override fun describeContents(): Int {
        return 0
    }



    companion object CREATOR : Parcelable.Creator<KeyValuePair> {
        override fun createFromParcel(parcel: Parcel): KeyValuePair {
            return KeyValuePair(parcel)
        }

        override fun newArray(size: Int): Array<KeyValuePair?> {
            return arrayOfNulls(size)
        }
    }
}