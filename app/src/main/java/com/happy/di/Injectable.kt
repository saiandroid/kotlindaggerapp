package com.happy.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable


interface UnInjectable