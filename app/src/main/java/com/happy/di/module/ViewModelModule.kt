package com.happy.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.happy.data.ArticleViewModel
import com.happy.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ArticleViewModel::class)
    abstract fun bindCardsViewModel(viewModel: ArticleViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: BoubyanViewModelFactory): ViewModelProvider.Factory
}