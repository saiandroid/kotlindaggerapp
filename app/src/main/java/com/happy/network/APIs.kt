package com.happy.network

import com.happy.data.model.Article
import com.happy.data.model.Envelope
import retrofit2.Call
import retrofit2.http.POST

interface APIs {

    /*
   * Global APIs
   */


    @POST("setting/logout")
    fun logout(): Call<Envelope<Any>>

    @POST("setting/status")
    fun getStatus(): Call<Envelope<Any>>

    @POST("setting/status")
    fun getArticles(): Call<Envelope<List<Article>>>




}