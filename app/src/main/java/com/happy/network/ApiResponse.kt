package com.happy.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.happy.data.Error
import com.happy.data.Resource
import com.happy.data.model.Envelope
import retrofit2.Response

sealed class ApiResponse<T> {
    companion object {
        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(error = com.happy.data.Error(title = error.message ?: "unknown error"))
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    if (body is Envelope<*> && body.error?.isClean != true) {
                        ApiErrorResponse(body.error)
                    } else {
                        ApiSuccessResponse(body = body)
                    }
                }
            } else {
                val msg = response.errorBody()?.string()
                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    msg
                }
                ApiErrorResponse(Error(title = errorMsg ?: "unknown error"))
            }
        }
    }
}

/**
 * separate class for HTTP 204 resposes so that we can make ApiSuccessResponse's body non-null.
 */
class ApiEmptyResponse<T> : ApiResponse<T>()
data class ApiSuccessResponse<T>(val body: T): ApiResponse<T>()
data class ApiErrorResponse<T>(val error: com.happy.data.Error?): ApiResponse<T>()

typealias ApiEnvelope<T> = ApiResponse<Envelope<T>>


fun <T> LiveData<ApiResponse<T>>.toResource(): LiveData<Resource<T>>? {
    return Transformations.map(this) {
        when (it) {
            is ApiSuccessResponse -> {
                Resource.success(it.body)
            }
            is ApiEmptyResponse -> {
                Resource.success(null)
            }
            is ApiErrorResponse -> {
                Resource.error(it.error, null)
            }
        }
    }
}

fun <T> LiveData<ApiResponse<Envelope<T>>>.toResourceData(): LiveData<Resource<T>>? {
    return Transformations.map(this) {
        when (it) {
            is ApiSuccessResponse -> {
                Resource.success(it.body.data)
            }
            is ApiEmptyResponse -> {
                Resource.success(null)
            }
            is ApiErrorResponse -> {
                Resource.error(it.error, null)
            }
        }
    }
}