package com.happy.network

import android.location.LocationManager
import android.os.Build
import com.bobuyan.example.demokotlin.BuildConfig
import com.happy.di.AppConfig
import okhttp3.*
import okio.Buffer
import okio.Okio
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class AppOkHttpInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        // hide error before every request
        val original = chain.request()

        val requestBuilder = original.newBuilder()
        requestBuilder.header("Accept", "application/json")
        requestBuilder.header("User-Agent", userAgent)
        requestBuilder.header("REQ-TK", UUID.randomUUID().toString())


        var requestBody: RequestBody? = null
        var hashString: String? = null
        if (original.method() == "POST") {
            val oldBody = original.body()

            val buffer = Buffer()
            oldBody?.writeTo(buffer)
            val strOldBody = buffer.readUtf8()

            var payload: String? = null
            try {
                var jsonObject: JSONObject = if (strOldBody != null && strOldBody.isNotEmpty()) JSONObject(strOldBody)
                else JSONObject()

                val securityJSONObject = JSONObject()
                securityJSONObject.put("deviceModel", Build.MANUFACTURER + ":" + Build.MODEL)
                securityJSONObject.put("osVersion", Build.VERSION.SDK_INT)
                securityJSONObject.put("requestToken", UUID.randomUUID().toString())

                jsonObject.put("security", securityJSONObject)
//                hashString = HmacSha512Signature.calculateRFC2104HMAC(jsonObject.toString(), AppConfig.HMAC_SHA1_KEY)
                payload = jsonObject.toString()


            } catch (e: JSONException) {
                e.printStackTrace()
            }


            val type = MediaType.parse("application/json; charset=utf-8")
            requestBody = RequestBody.create(type, payload)
            requestBuilder.header("Content-Type", requestBody?.contentType()?.toString())
            requestBuilder.header("Content-Length", requestBody?.contentLength()?.toString())
            requestBuilder.header("disableEncryptData", "false")// true -> disable server decryption
//            requestBuilder.header("RHV", hashString)

        }


        requestBuilder.method(original.method(), requestBody)

        val request = requestBuilder.build()

        val response = chain.proceed(request)

        return if (response.isSuccessful && response.body() != null && response.body()?.source() != null) {
            val body = Okio.buffer(response.body()?.source()).readUtf8()

            if (body != null) {
                val type = MediaType.parse("application/json; charset=utf-8")
                response.newBuilder().body(ResponseBody.create(type, body)).build()
            } else {
                response
            }
        } else {
            response
        }

    }


    private val userAgent: String
        get() {
            val osVersion = Build.VERSION.SDK_INT
            val device = Build.MANUFACTURER + ":" + Build.MODEL
            val appVersion = BuildConfig.VERSION_NAME

               return "Mobile/$appVersion (Test,Android version/$osVersion, $device)"

        }

}