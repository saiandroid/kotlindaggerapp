package com.happy.network

import android.os.Parcel
import android.os.Parcelable
import com.happy.data.model.Article
import com.happy.data.model.KeyValuePair

class ConfirmationData() : Parcelable {
    var confirmationItems: List<KeyValuePair>? = null
    var toCard: Article? = null


    constructor(parcel: Parcel) : this() {
        toCard = parcel.readParcelable(Article::class.java.classLoader)

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(toCard, flags)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<ConfirmationData> {
            override fun createFromParcel(parcel: Parcel) = ConfirmationData(parcel)

            override fun newArray(size: Int) = arrayOfNulls<ConfirmationData>(size)
        }
    }
}