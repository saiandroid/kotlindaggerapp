package com.happy.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.net.ssl.*

class NetworkRepository  @Inject constructor( private val apIs: APIs
    ) {


    fun getArticles() = NetworkRequestHandler.doApiRequest(apIs.getArticles()).toResource()




}