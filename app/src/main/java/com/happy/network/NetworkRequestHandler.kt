package com.happy.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.happy.AppEvents
import com.happy.data.model.Envelope
import com.happy.eventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object NetworkRequestHandler {

    fun <R> doApiRequest(makeRequest: Call<R>, showLoading:Boolean = true): LiveData<ApiResponse<R>> {

        //Event Subscribed in BaseActivity
        if (showLoading) eventBus.post(AppEvents.StartLoading.œ())
        val responseLiveData = MutableLiveData<ApiResponse<R>>()

        makeRequest.enqueue(object : Callback<R> {
            override fun onFailure(call: Call<R>, t: Throwable) {
                responseLiveData.value = ApiResponse.create(t)
            }

            override fun onResponse(call: Call<R>, response: Response<R>) {
                responseLiveData.value = ApiResponse.create(response)

                (response.body() as? Envelope<*>)?.let {

                    if (it.error  == null) eventBus.post(AppEvents.Exception.œ())
                }
            }

        })

        return responseLiveData
    }

}