package com.happy.data

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.happy.data.model.Article
import com.happy.data.model.Envelope
import com.happy.network.NetworkRepository
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class ArticleViewModelTest{

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val networkRepository = mock(NetworkRepository::class.java)
    private val context = mock(Application::class.java)
    private val viewModel = ArticleViewModel(networkRepository)


    @Test
    fun testGetArticles() {
        val foo = MutableLiveData<Resource<Envelope<List<Article>>>>()

        Mockito.`when`(networkRepository.getArticles()).thenReturn(foo)

        viewModel.getArticles()

        Mockito.verify(networkRepository).getArticles()

        Mockito.verifyNoMoreInteractions(networkRepository)
    }

}